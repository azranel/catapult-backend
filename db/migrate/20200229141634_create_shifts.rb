class CreateShifts < ActiveRecord::Migration[6.0]
  def change
    create_table :shifts do |t|
      t.date :shift_date
      t.datetime :start_time
      t.datetime :end_time
      t.integer :staff_required
      t.integer :number_of_invited_staff
      t.string :job_type

      t.timestamps
    end
  end
end
