class ChangeDefaultForShiftNumberOfInvitedPeople < ActiveRecord::Migration[6.0]
  def up
    change_column :shifts, :number_of_invited_staff, :integer, default: 0
  end

  def down
    change_column :shifts, :number_of_invited_staff, :integer, default: nil
  end
end
