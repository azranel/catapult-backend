class CreateContracts < ActiveRecord::Migration[6.0]
  def change
    create_table :contracts do |t|
      t.references :shift, null: false
      t.string :candidate_name, null: false

      t.timestamps
    end
  end
end
