# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

JOB_TYPES = ['Security', 'Receptionist', 'Barista', 'Retail store shift', 'Waiting staff']

1.upto(6) do |sequence_number|
  shift_date = Time.now + sequence_number.days
  start_time = shift_date.at_beginning_of_hour
  end_time = start_time + 4.hours
  contract_numbers = sequence_number - 1

  shift = Shifts::ShiftRepository.create(
    shift_date: shift_date,
    start_time: start_time,
    end_time: end_time,
    staff_required: sequence_number,
    job_type: JOB_TYPES[sequence_number % JOB_TYPES.length]
  )

  (0).upto(contract_numbers) do
    Shifts::ContractsRepository.create(
      shift_id: shift.id,
      candidate_name: "John Doe"
    )
  end
end
