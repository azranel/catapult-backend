require 'rails_helper'

describe Shifts::ContractsRepository do
  describe '.list' do
    subject { described_class.list(params) }
    context 'without any params' do
      let(:params) { {} }
      let!(:contracts) { create_list(:contract, 5) }

      it 'fetches all the contracts' do
        expect(subject).to eq contracts
      end
    end

    context 'with shift_id param' do
      let(:params) { { shift_id: shift.id } }
      let(:shift) { create(:shift) }
      let!(:shift_contract) { create(:contract, shift_id: shift.id) }
      let!(:non_shift_contract) { create(:contract) }

      it 'fetches only contract for specified shift' do
        expect(subject).to eq([shift_contract])
      end
    end
  end

  describe '.create' do
    let(:shift) { create(:shift) }
    let(:params) { { shift_id: shift.id, candidate_name: "John Doe" } }
    subject { described_class.create(params) }

    it 'creates the contract' do
      expect { subject }.to change { Contract.count }.by(1)
    end

    it 'increments number of invited contracts in shift' do
      expect(shift.number_of_invited_staff).to eq(0)
      subject
      expect(shift.reload.number_of_invited_staff).to eq(1)
    end
  end
end
