require 'rails_helper'

describe Shift do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:shift_date)  }
    it { is_expected.to validate_presence_of(:start_time) }
    it { is_expected.to validate_presence_of(:end_time) }
    it { is_expected.to validate_presence_of(:staff_required) }

    it 'validates that end time is after start time' do
      shift = FactoryBot.build(:shift)
      shift.start_time = shift.end_time + 4.hours
      expect(shift).not_to be_valid
    end

    it 'validates that number_of_invited_staff is smaller than staff_required' do
      shift = FactoryBot.build(:shift)
      shift.number_of_invited_staff = shift.staff_required + 10
      expect(shift).not_to be_valid
    end
  end
end
