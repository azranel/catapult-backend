require 'rails_helper'

describe Api::ContractsController do
  describe 'GET #index' do
    subject { get :index, params: params }

    context 'without any parameters' do
      let(:params) { {} }
      let!(:contracts) { create_list(:contract, 5) }
      let(:contracts_json) { contracts.map { |c| ContractSerializer.as_json(c).stringify_keys } }

      it 'fetches all the contracts' do
        subject
        expect(json_response.length).to eq 5
        expect(json_response).to eq contracts_json
      end
    end

    context 'with shift_id parameter' do
      let(:params) { { role_id: shift.id } }
      let(:shift) { create(:shift) }
      let!(:contract) { create(:contract, shift_id: shift.id) }
      let!(:other_contract) { create(:contract) }

      it 'fetches only contract for given shift' do
        subject
        expect(json_response.length).to eq(1)
        expect(json_response.first).to eq(ContractSerializer.as_json(contract).stringify_keys)
      end
    end
  end
end
