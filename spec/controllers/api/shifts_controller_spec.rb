require 'rails_helper'

describe Api::ShiftsController do
  describe 'GET #index' do
    subject { get :index, params: params }

    context 'without any params' do
      let(:params) { {} }
      let!(:shifts) { create_list(:shift, 5) }
      let(:shifts_json) { shifts.map { |shift| ShiftSerializer.as_json(shift).stringify_keys } }

      it 'fetches all the shifts' do
        subject
        expect(json_response.length).to eq shifts.length
        expect(json_response).to eq(shifts_json)
      end
    end

    context 'with job_type parameter' do
      let(:params) { { job_type: job_type } }
      let(:job_type) { 'Security' }
      let!(:security_shift) { create(:shift, job_type: job_type) }
      let!(:non_security_shift) { create(:shift, job_type: 'Receptionist') }

      it 'fetches only the Security shifts' do
        subject
        expect(json_response.length).to eq 1
        expect(json_response.first).to eq(ShiftSerializer.as_json(security_shift).stringify_keys)
      end
    end

    context 'with start_time parameter' do
      let(:params) { { start_time: start_time } }
      let(:start_time) { Time.now }
      let!(:shift_after_start_time) { create(:shift, start_time: start_time + 5.days) }
      let!(:shift_before_start_time) { create(:shift, start_time: start_time - 5.days) }

      it 'fetches only shifts that start after start_time parameter' do
        subject
        expect(json_response.length).to eq 1
        expect(json_response.first).to eq(ShiftSerializer.as_json(shift_after_start_time).stringify_keys)
      end
    end

    context 'with start_time and job_type parameter' do
      let(:params) { { job_type: job_type, start_time: start_time } }
      let(:job_type) { 'Security' }
      let(:start_time) { Time.now + 1.month }
      let!(:security_shift) { create(:shift, job_type: job_type) }
      let!(:shift_after_start_time) { create(:shift, job_type: 'Receptionist', start_time: start_time + 5.days) }
      let!(:security_shift_after_start_time) { create(:shift, start_time: start_time + 2.days, job_type: job_type) }

      it 'fetches only security job that start after start_time parameter' do
        subject
        expect(json_response.length).to eq 1
        expect(json_response.first).to eq(ShiftSerializer.as_json(security_shift_after_start_time).stringify_keys)
      end
    end
  end
end
