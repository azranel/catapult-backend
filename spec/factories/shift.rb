FactoryBot.define do
  factory :shift do
    shift_date { Time.now + 5.days }
    start_time { shift_date.at_beginning_of_hour }
    end_time { start_time + 4.hours }
    staff_required { rand(1..8) }
    job_type { 'Security' }
  end
end
