FactoryBot.define do
  factory :contract do
    shift
    candidate_name { 'John Doe' }
  end
end
