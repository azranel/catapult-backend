class ContractSerializer < BaseSerializer
  attributes :id, :roleId, :candidateName

  def roleId
    @model.shift_id
  end

  def candidateName
    @model.candidate_name
  end
end
