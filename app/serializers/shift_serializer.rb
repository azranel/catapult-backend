class ShiftSerializer < BaseSerializer
  DATE_FORMAT = "%A %d %B %Y"
  TIME_FORMAT = "%H:%M"
  attributes :roleId, :shiftDate, :startTime, :endTime, :staff_required, :number_of_invited_staff, :jobType

  def roleId
    @model.id
  end

  def shiftDate
    @model.shift_date.strftime(DATE_FORMAT)
  end

  def startTime
    @model.start_time.strftime(TIME_FORMAT)
  end

  def endTime
    @model.end_time.strftime(TIME_FORMAT)
  end

  def jobType
    @model.job_type
  end
end
