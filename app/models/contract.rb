class Contract < ApplicationRecord
  belongs_to :shift

  validates :shift_id, :candidate_name, presence: true
end
