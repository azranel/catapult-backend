class Shift < ApplicationRecord
  validates :shift_date, :start_time, :end_time, :staff_required, presence: true
  validate :end_time_is_after_start_time
  validate :number_of_invited_staff_is_smaller_than_staff_required

  private

  def end_time_is_after_start_time
    return if end_time.nil? || start_time.nil?
    if end_time < start_time
      errors.add(:end_time, 'needs to be before start time')
    end
  end

  def number_of_invited_staff_is_smaller_than_staff_required
    return if staff_required.nil? || number_of_invited_staff.nil?
    if staff_required < number_of_invited_staff
      errors.add(:number_of_invited_staff, 'cannot invite more people than there is a requirement')
    end
  end
end
