module Shifts
  # Repository responsible for all read and write operations on shifts
  class ShiftRepository
    def self.list(params = {})
      shifts = Shift
      shifts = shifts.where(job_type: params[:job_type]) if params[:job_type]
      if params[:start_time]
        parsed_start_time = Time.parse(params[:start_time])
        shifts = shifts.where('start_time >= ?', parsed_start_time)
      end
      shifts.all
    end

    def self.find(id:)
      Shift.find(id)
    end

    def self.create(params)
      shift = Shift.new(params)
      shift.save if shift.valid?
      shift
    end
  end
end
