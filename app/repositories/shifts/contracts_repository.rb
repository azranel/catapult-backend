module Shifts
  class ContractsRepository
    def self.list(params)
      contracts = Contract
      contracts = contracts.where(shift_id: params[:shift_id]) if params[:shift_id]
      contracts.all
    end

    def self.create(shift_id:, candidate_name:)
      shift = Shifts::ShiftRepository.find(id: shift_id)
      contract = Contract.create!(
        shift_id: shift_id,
        candidate_name: candidate_name
      )
      shift.increment(:number_of_invited_staff) && shift.save!
      contract
    end
  end
end
