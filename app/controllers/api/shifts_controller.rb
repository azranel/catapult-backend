module Api
  class ShiftsController < ApplicationController

    def index
      list_params = params.permit(:job_type, :start_time)
      shifts = Shifts::ShiftRepository.list(params)
      shifts_json = shifts.map { |shift| ShiftSerializer.as_json(shift) }
      render json: shifts_json
    end

  end
end
