module Api
  class ContractsController < ApplicationController
    def index
      list_params = { shift_id: params[:role_id] }
      contracts = Shifts::ContractsRepository.list(list_params)
      contracts_json = contracts.map { |contract| ContractSerializer.as_json(contract) }
      render json: contracts_json
    end
  end
end
