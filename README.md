# README

## Requirements

Ruby 2.7.0
Rails 6.0.2
SQLite 3 - I used this only for sake of the test, normally I would go with postgres since I know it the most and also I personally prefer it over MySQL

## Installation

```
bundle
rails db:setup
rails s
```

## Testing

```
rspec
```
